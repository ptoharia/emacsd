#!/bin/bash
newPwd=$1
if [ ! -d "$newPwd" ]; then
    echo "Error: dir " $newPwd " does not exist"
    exit 1
fi
cd $1
shift
echo "Running from " `pwd`
clang "$@"
