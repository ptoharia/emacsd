#!/bin/bash
rm -rf ~/opt/rtags/ ~/opt/src/rtags*
sudo apt install libcppunit-dev
cd $HOME/opt/src
git clone https://github.com/Andersbakken/rtags.git
cd rtags
git checkout v2.21
git submodule update --init
cmake . -DCMAKE_INSTALL_PREFIX=~/opt/rtags -DCMAKE_BUILD_TYPE=Release
make install -j 
