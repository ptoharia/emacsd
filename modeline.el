;;;
;;; Code:
(use-package powerline
  :ensure t
  :config
  (powerline-default-theme))

;; (use-package airline-themes
;;   :ensure t)

;; (load-theme 'airline-cool)

(use-package uniquify
  :config
  (setq uniquify-buffer-name-style 'forward)
  (setq uniquify-separator "/")
  ;; rename after killing uniquified
  (setq uniquify-after-kill-buffer-p t)
  ;; don't muck with special buffers
  (setq uniquify-ignore-buffers-re "^\\*"))


;;; modeline.el ends here
