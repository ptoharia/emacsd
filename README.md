* Rtags installation
```
sudo apt-get install llvm libssl-dev libclang-4.0-dev
mkdir -p ~/opt/src && cd ~/opt/src
git clone --recursive https://github.com/Andersbakken/rtags.git
cd rtags
# Set LIBCLANG_LLVM_CONFIG_EXECUTABLE if specific llvm-config needed
# LIBCLANG_LLVM_CONFIG_EXECUTABLE=`which llvm-config-4.0` 
cmake . -DCMAKE_INSTALL_PREFIX=$HOME/opt/rtags
make install -j
echo export PATH=$PATH:~/opt/rtags/bin >> ~/.bashrc
```

* Irony
```
(M-x) irony-install-server
```
