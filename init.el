;;; .emacs --- ptoharia's .emacs
;;; Commentary:
;; ptoharia's .emacs

(require 'package)

;;; Code:

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
;; keep the installed packages in .emacs.d
(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))
(package-initialize)
;; update the package metadata is the local cache is missing
(unless package-archive-contents
  (package-refresh-contents))


;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;; Use monokai theme
(use-package monokai-theme
  :ensure t
  )

(use-package vscode-dark-plus-theme
  :ensure t
  )


;; (defun which-linux-distribution()
;;   "From lsb_release."
;;   (interactive)
;;   (when (eq system-type 'gnu/linux)
;;      (shell-command-to-string "lsb_release -si")))

(defvar linux-lsb-release "none")

(when (eq system-type 'gnu/linux)
  (setq linux-lsb-release (shell-command-to-string "lsb_release -si"))
)

(when (string-match-p "Ubuntu" linux-lsb-release)
  (set-frame-font "UbuntuMono:pixelsize=34")
)


(when (eq system-type 'darwin)
  (set-frame-font "Menlo:pixelsize=14")
)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; Always load newest byte code
(setq load-prefer-newer t)

;; reduce the frequency of garbage collection by making it happen on
;; each 50MB of allocated data (the default is on every 0.76MB)
(setq gc-cons-threshold 50000000)

;; warn when opening files bigger than 100MB
(setq large-file-warning-threshold 100000000)


(defconst ptoharia-savefile-dir
  (expand-file-name "savefile" user-emacs-directory))


(setq load-path
      (cons (expand-file-name "cmake-mode" user-emacs-directory) load-path))
(require 'cmake-mode)


;; create the savefile dir if it doesn't exist
(unless (file-exists-p ptoharia-savefile-dir)
  (make-directory ptoharia-savefile-dir))

;; the toolbar is just a waste of valuable screen estate
;; in a tty tool-bar-mode does not properly auto-load, and is
;; already disabled anyway
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

(menu-bar-mode -1)

;; the blinking cursor is nothing, but an annoyance
(blink-cursor-mode -1)

;; disable the annoying bell ring
(setq ring-bell-function 'ignore)

;; disable startup screen
(setq inhibit-startup-screen t)

;; nice scrolling
;; (setq scroll-margin 0
;;       scroll-conservatively 100000
;;       scroll-preserve-screen-position 1)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
;;(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-conservatively 10000)
(setq scroll-step 1) ;; keyboard scroll one line at a time

;; enable y/n answers
(fset 'yes-or-no-p 'y-or-n-p)

;; more useful frame title, that show either a file or a
;; buffer name (if the buffer isn't visiting a file)
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;; Emacs modes typically provide a standard means to change the
;; indentation width -- eg. c-basic-offset: use that to adjust your
;; personal indentation width, while maintaining the style (and
;; meaning) of any files you load.
(setq-default indent-tabs-mode nil)   ;; don't use tabs to indent
(setq-default tab-width 2)            ;; but maintain correct appearance

;; Newline at end of file
(setq require-final-newline t)

;; delete the selection with a keypress
(delete-selection-mode t)

;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; revert buffers automatically when underlying files are changed externally
(global-auto-revert-mode t)



(use-package rtags
  :ensure t
  :config
  (rtags-enable-standard-keybindings)
  )
;;   Shortcuts
;;   (define-key map (kbd (concat prefix ".")) 'rtags-find-symbol-at-point)
;;   (define-key map (kbd (concat prefix ",")) 'rtags-find-references-at-point)
;;   (define-key map (kbd (concat prefix "v")) 'rtags-find-virtuals-at-point)
;;   (define-key map (kbd (concat prefix "V")) 'rtags-print-enum-value-at-point)
;;   (define-key map (kbd (concat prefix "/")) 'rtags-find-all-references-at-point)
;;   (define-key map (kbd (concat prefix "Y")) 'rtags-cycle-overlays-on-screen)
;;   (define-key map (kbd (concat prefix ">")) 'rtags-find-symbol)
;;   (define-key map (kbd (concat prefix "<")) 'rtags-find-references)
;;   (define-key map (kbd (concat prefix "[")) 'rtags-location-stack-back)
;;   (define-key map (kbd (concat prefix "]")) 'rtags-location-stack-forward)
;;   (define-key map (kbd (concat prefix "D")) 'rtags-diagnostics)
;;   (define-key map (kbd (concat prefix "C")) 'rtags-compile-file)
;;   (define-key map (kbd (concat prefix "G")) 'rtags-guess-function-at-point)
;;   (define-key map (kbd (concat prefix "p")) 'rtags-dependency-tree)
;;   (define-key map (kbd (concat prefix "P")) 'rtags-dependency-tree-all)
;;   (define-key map (kbd (concat prefix "e")) 'rtags-reparse-file)
;;   (define-key map (kbd (concat prefix "E")) 'rtags-preprocess-file)
;;   (define-key map (kbd (concat prefix "R")) 'rtags-rename-symbol)
;;   (define-key map (kbd (concat prefix "M")) 'rtags-symbol-info)
;;   (define-key map (kbd (concat prefix "U")) 'rtags-display-summary-as-message)
;;   (define-key map (kbd (concat prefix "S")) 'rtags-display-summary)
;;   (define-key map (kbd (concat prefix "O")) 'rtags-goto-offset)
;;   (define-key map (kbd (concat prefix ";")) 'rtags-find-file)
;;   (define-key map (kbd (concat prefix "F")) 'rtags-fixit)
;;   (define-key map (kbd (concat prefix "L")) 'rtags-copy-and-print-current-location)
;;   (define-key map (kbd (concat prefix "X")) 'rtags-fix-fixit-at-point)
;;   (define-key map (kbd (concat prefix "B")) 'rtags-show-rtags-buffer)
;;   (define-key map (kbd (concat prefix "K")) 'rtags-make-member)
;;   (define-key map (kbd (concat prefix "I")) 'rtags-imenu)
;;   (define-key map (kbd (concat prefix "T")) 'rtags-taglist)
;;   (define-key map (kbd (concat prefix "h")) 'rtags-print-class-hierarchy)
;;   (define-key map (kbd (concat prefix "a")) 'rtags-print-source-arguments)
;;   (define-key map (kbd (concat prefix "A")) 'rtags-find-functions-called-by-this-function)
;;   (define-key map (kbd (concat prefix "l")) 'rtags-list-results))


;; (use-package flycheck
;;   :ensure t
;;   )
(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-gcc-language-standard "c++11")))
  ;;(setq-default flycheck-disabled-checkers '(c/c++-clang))
  ;;(setq-default flycheck-disabled-checkers '(c/c++-gcc))
  )

;; (use-package flycheck-clangcheck
;;   :ensure t
;;   :config
;;   (setq flycheck-clangcheck-analyze t)
;;   (add-hook 'c++-mode-hook
;;             (lambda () (flycheck-select-checker 'c/c++-clangcheck)))
;;   )


(use-package auto-complete-clang
  :ensure t
  )

(define-key ac-mode-map  [ (kbd "<s-tab>") ] 'auto-complete)


(use-package company
  :ensure t
  :config
  (global-company-mode)
)

;; (use-package company-clang
;;   :ensure t
;; )

(use-package irony
  :ensure t
)


;; cd $HOME/opt
;; git clone --recursive https://github.com/Andersbakken/rtags.git
;; cd rtags
;; cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
;; make -j4
;; # Meter en el path
;; # export PATH=$PATH:~/bin:~/opt/rtags/bin
(use-package cmake-ide
  :ensure t
  :config
  (cmake-ide-setup)
  )
(cmake-ide-setup)

;; hippie expand is dabbrev expand on steroids
(setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                         try-expand-dabbrev-all-buffers
                                         try-expand-dabbrev-from-kill
                                         try-complete-file-name-partially
                                         try-complete-file-name
                                         try-expand-all-abbrevs
                                         try-expand-list
                                         try-expand-line
                                         try-complete-lisp-symbol-partially
                                         try-complete-lisp-symbol))

;; use hippie-expand instead of dabbrev
(global-set-key (kbd "M-/") #'hippie-expand)
(global-set-key (kbd "s-/") #'hippie-expand)
(global-set-key (kbd "<C-tab>") #'hippie-expand)

;; replace buffer-menu with ibuffer
(global-set-key (kbd "C-x C-b") #'ibuffer)

;; ;; align code in a pretty way
;; (global-set-key (kbd "C-x \\") #'align-regexp)

;; ;; extend the help commands
;; (define-key 'help-command (kbd "C-f") #'find-function)
;; (define-key 'help-command (kbd "C-k") #'find-function-on-key)
;; (define-key 'help-command (kbd "C-v") #'find-variable)
;; (define-key 'help-command (kbd "C-l") #'find-library)

;; (define-key 'help-command (kbd "C-i") #'info-display-manual)

;; ;; misc useful keybindings
;; (global-set-key (kbd "s-<") #'beginning-of-buffer)
;; (global-set-key (kbd "s->") #'end-of-buffer)
;; (global-set-key (kbd "s-q") #'fill-paragraph)

;; ;; smart tab behavior - indent or complete
;; (setq tab-always-indent 'complete)

;; (unless (package-installed-p 'use-package)
;;   (package-install 'use-package))

;; (require 'use-package)
;; (setq use-package-verbose t)

(setq compile-command "make -C ~/prj/" )
(setq compilation-scroll-output 'first-error)
(setq c-default-style "bsd"
          c-basic-offset 2)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))


;; make whitespace-mode use just basic coloring
;;(setq whitespace-style (quote (tabs newline space-mark tab-mark newline-mark)))
(setq whitespace-style '(face trailing lines empty indentation::space))
(setq whitespace-line-column 800)
;; ;; (setq whitespace-mode 1)
(global-whitespace-mode 0)


(use-package stripe-buffer              ; Add stripes to a buffer
;;  :disabled t
  :ensure t
  :init (add-hook 'dired-mode-hook #'stripe-buffer-mode))


(use-package fill-column-indicator
  :ensure t
  :config
  (setq fci-rule-column 80)
  (setq fci-rule-width 1)
  ;(setq fci-rule-use-dashes 1)
  (setq fci-rule-color "DarkSlateGray"))
;(add-hook 'after-change-major-mode-hook 'fci-mode)
(add-hook 'c-mode-hook 'fci-mode)
(add-hook 'c++-mode-hook 'fci-mode)
(add-hook 'cmake-mode-hook 'fci-mode)

(global-set-key [f12] 'compile)
(global-set-key (kbd "M-s-b") 'compile)
(global-set-key [f5] 'revert-buffer)

;; ;;(set-face-attribute 'default nil :height 120)
;; ;;(set-face-attribute 'default nil :family "Inconsolata" :height 120)


(use-package ido
  :ensure t
  :config
  (setq ido-enable-prefix nil
        ido-enable-flex-matching t
        ido-create-new-buffer 'always
        ido-use-filename-at-point 'guess
        ido-max-prospects 10
        ido-save-directory-list-file (expand-file-name "ido.hist" ptoharia-savefile-dir)
        ido-default-file-method 'selected-window
        ido-auto-merge-work-directories-length -1)
  (ido-mode +1))

;;(use-package ido-ubiquitous
(use-package ido-completing-read+
  :ensure t
  :config
  (ido-ubiquitous-mode +1))

(use-package smex
  :ensure t
  :config
  (global-set-key (kbd "M-x") 'smex)
  (global-set-key (kbd "M-X") 'smex-major-mode-commands)
  ;; This is your old M-x.
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command))


(use-package savehist
  :config
  (setq savehist-additional-variables
        ;; search entries
        '(search-ring regexp-search-ring)
        ;; save every minute
        savehist-autosave-interval 60
        ;; keep the home clean
        savehist-file (expand-file-name "savehist" ptoharia-savefile-dir))
  (savehist-mode +1))

(use-package recentf
  :config
  (setq recentf-save-file (expand-file-name "recentf" ptoharia-savefile-dir)
        recentf-max-saved-items 500
        recentf-max-menu-items 15
        ;; disable recentf-cleanup on Emacs start, because it can cause
        ;; problems with remote files
        recentf-auto-cleanup 'never)
  (recentf-mode +1))
(global-set-key "\C-x\ \C-r" 'recentf-open-files)


(use-package windmove
  :config
  ;; use shift + arrow keys to switch between visible buffers
  ;; (windmove-default-keybindings)
)

(global-set-key (kbd "M-<left>") #'windmove-left)
(global-set-key (kbd "M-<right>") #'windmove-right)
(global-set-key (kbd "M-<up>") #'windmove-up)
(global-set-key (kbd "M-<down>") #'windmove-down)


;;sudo apt-get install aspell-es
(use-package flyspell
  :config
  (when (eq system-type 'windows-nt)
    (add-to-list 'exec-path "C:/Program Files (x86)/Aspell/bin/"))
  (setq ispell-program-name "aspell" ; use aspell instead of ispell
        ispell-extra-args '("--sug-mode=ultra"))
  (setq flyspell-default-dictionary "castellano8")
  (add-hook 'text-mode-hook #'flyspell-mode)
  (add-hook 'prog-mode #'flyspell-prog-mode)
  (add-hook 'latex-mode-hook 'flyspell-mode))

(defun fd-switch-dictionary()
      (interactive)
      (let* ((dic ispell-current-dictionary)
    	 (change (if (string= dic "castellano") "english" "castellano")))
        (ispell-change-dictionary change)
        (message "Dictionary switched from %s to %s" dic change)
        ))
      (global-set-key (kbd "<f8>")   'fd-switch-dictionary)

(use-package flyspell-popup
  :ensure t
  :config
  (define-key flyspell-mode-map (kbd "C-;") #'flyspell-popup-correct)
  ;; (add-hook 'flyspell-mode-hook #'flyspell-popup-auto-correct-mode)
  )


(use-package minimap
  :ensure t
  :bind (("C-<f1>" . minimap-mode)))



;; ==========================================
;; (optional) bind M-o for ff-find-other-file
;; ==========================================
(defvar my-cpp-other-file-alist '(("\\.cpp\\'" (".h" ".hpp" ".ipp"))
                                  ("\\.ipp\\'" (".hpp" ".cpp"))
                                  ("\\.hpp\\'" (".ipp" ".cpp"))
                                  ("\\.cxx\\'" (".hxx" ".ixx"))
                                  ("\\.ixx\\'" (".cxx" ".hxx"))
                                  ("\\.hxx\\'" (".ixx" ".cxx"))
                                  ("\\.c\\'" (".h"))
                                  ("\\.h\\'" (".cpp" "c"))))
(setq-default ff-other-file-alist 'my-cpp-other-file-alist)
(add-hook 'c-mode-common-hook (lambda () (define-key c-mode-base-map
                                           [(meta o)] 'ff-get-other-file)))


;; (setq irony-additional-clang-options '("-std=c++11"))

(use-package highlight-symbol
  :ensure t
  :init
  (global-set-key [(control f3)] 'highlight-symbol)
  (global-set-key [f3] 'highlight-symbol-next)
  (global-set-key [(shift f3)] 'highlight-symbol-prev)
  (global-set-key [(meta f3)] 'highlight-symbol-query-replace))

(use-package smart-comment
  :ensure t
  :bind ("M-;" . smart-comment))


;; Function to go to the beggining of a word being search with isearch hitting
;; Control+Return
(defun isearch-exit-other-end (rbeg rend)
    "Exit isearch, but at the other end of the search string.
  This is useful when followed by an immediate kill."
    (interactive "r")
    (isearch-exit)
    (goto-char isearch-other-end))

(define-key isearch-mode-map [(control return)] 'isearch-exit-other-end)


;; highlights cursor when scrolling
(use-package beacon
  :ensure t
  :init
  (beacon-mode 1)
  (setq beacon-push-mark 35)
  (setq beacon-color "#8888aa"))


;; (use-package ggtags
;;   :ensure t
;;   :init
;;   (add-hook 'c++-mode-hook 'ggtags-mode)
;; )

;; To eliniate borrar and nuevo tags
(global-set-key (kbd "s-b") (lambda () (interactive)
                              (query-replace-regexp "\\\\delPT{[^}]*}" "" )))
(global-set-key (kbd "s-n") (lambda () (interactive)
                              (query-replace-regexp "\\\\newPT{\\([^}]*\\)}" "\\1" )))

(use-package julia-mode
  :ensure t
  )

(use-package ess-site
  :ensure ess
  :mode ("\\.R\\'" . R-mode)
  :commands R
  :config
  (add-hook 'R-mode-hook #'subword-mode)
  (add-hook 'R-mode-hook #'smartparens-strict-mode)
  )



(defun tag-borrar (ξfrom ξto)
  (interactive "r")
  (goto-char ξto)
  (insert "}")
  (goto-char ξfrom)
  (insert "\\delPT{")
  (goto-char ξto)
  (forward-char 9 )
  )
(defun tag-nuevo ()
  (interactive)
  (insert "\\newPT{}")
  (left-char)
  )
(defun tag-borrar-nuevo (ξfrom ξto)
  (interactive "r")
  (tag-borrar ξfrom ξto)
  (insert "\\newPT{}")
  (left-char)
  )
(defun tag-textbf (ξfrom ξto)
  (interactive "r")
  (goto-char ξto)
  (insert "}")
  (goto-char ξfrom)
  (insert "\\textbf{")
  (goto-char ξto)
  (forward-char 9 )
  )
(global-set-key (kbd "s-<backspace>") 'tag-borrar)
(global-set-key (kbd "s-<insert>") 'tag-nuevo)
(global-set-key (kbd "M-s-<backspace>") 'tag-borrar-nuevo)

(global-set-key (kbd "C-s-n") 'tag-textbf)


(use-package php-mode
  :ensure t
  )
(add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))


;; Set fill-column 80
(setq-default fill-column 80)

(setq cmake-ide-build-dir "")

(defun cmake-ide-get-build-dir ( checker )
  "Return cmake ide build dir CHECKER."
  ;; "/home/ptoharia/prj/NeuroScheme/build"
  cmake-ide-build-dir
  )

(defun cmake-ide-run-cmake-and-reload-db ()
  "Rerun cmake, clean the compile-commands.json and reloads the db."
  (interactive)
  ;;(cmake-ide-run-cmake)
  (shell-command (concat (substitute-in-file-name "$HOME/.emacs.d/cleanCMakeCompileCommands.sh") cmake-ide-build-dir))
  ;;(call-process "/home/ptoharia/.emacs.d/cleanCMakeCompileCommands.sh" cmake-ide-build-dir)
  (cmake-ide-load-db)
  )

(defun cmake-ide-set-build-dir-and-run-cmake-and-reload-db ( new-build-dir )
  "Set cmake-ide-build-dir to NEW-BUILD-DIR, reruns cmake, cleans the compile-commands.json and reloads the db."
  (interactive "DBuild dir: ")
  (cmake-ide-run-cmake)
  (setq cmake-ide-build-dir new-build-dir)
  (shell-command (concat (substitute-in-file-name "$HOME/.emacs.d/cleanCMakeCompileCommands.sh") cmake-ide-build-dir))
  (cmake-ide-load-db)
  )

(setq clangemacs (substitute-in-file-name "$HOME/.emacs.d/clang.emacs"))

(flycheck-define-checker c/c++-clangold
  "A C/C++ syntax checker using Clang.

;; To use OpenMP with clang
;;  sudo apt-get install libomp-dev

See URL `http://clang.llvm.org/'."
  :command ("/home/ptoharia/.emacs.d/clang.emacs"
            (eval cmake-ide-build-dir)

            "-fno-color-diagnostics"    ; Do not include color codes in output
            "-fno-caret-diagnostics"    ; Do not visually indicate the source
                                        ; location
            "-fopenmp"
            "-fno-diagnostics-show-option" ; Do not show the corresponding
                                        ; warning group
            "-iquote" (eval (flycheck-c/c++-quoted-include-directory))
            (option "-std=" flycheck-clang-language-standard concat)
            (option-flag "-pedantic" flycheck-clang-pedantic)
            (option-flag "-pedantic-errors" flycheck-clang-pedantic-errors)
            (option "-stdlib=" flycheck-clang-standard-library concat)
            (option-flag "-fms-extensions" flycheck-clang-ms-extensions)
            (option-flag "-fno-exceptions" flycheck-clang-no-exceptions)
            (option-flag "-fno-rtti" flycheck-clang-no-rtti)
            (option-flag "-fblocks" flycheck-clang-blocks)
            (option-list "-include" flycheck-clang-includes)
            (option-list "-W" flycheck-clang-warnings concat)
            (option-list "-D" flycheck-clang-definitions concat)
            (option-list "-I" flycheck-clang-include-path)
            (eval flycheck-clang-args)
            "-x" (eval
                  (pcase major-mode
                    (`c++-mode "c++")
                    (`c-mode "c")))
            ;; Read from standard input
            "-")
  :standard-input t
  :error-patterns
  ((error line-start
          (message "In file included from") " " (or "<stdin>" (file-name))
          ":" line ":" line-end)
   (info line-start (or "<stdin>" (file-name)) ":" line ":" column
         ": note: " (optional (message)) line-end)
   (warning line-start (or "<stdin>" (file-name)) ":" line ":" column
            ": warning: " (optional (message)) line-end)
   (error line-start (or "<stdin>" (file-name)) ":" line ":" column
          ": " (or "fatal error" "error") ": " (optional (message)) line-end))
  :error-filter
  (lambda (errors)
    (let ((errors (flycheck-sanitize-errors errors)))
      (dolist (err errors)
        ;; Clang will output empty messages for #error/#warning pragmas without
        ;; messages.  We fill these empty errors with a dummy message to get
        ;; them past our error filtering
        (setf (flycheck-error-message err)
              (or (flycheck-error-message err) "no message")))
      (flycheck-fold-include-levels errors "In file included from")))
  :modes (c-mode c++-mode)
  :working-directory cmake-ide-get-build-dir
  ;; :dddefault-directory cmake-ide-get-build-dir
  :next-checkers ((warning . c/c++-cppcheck)))


(setq ac-clang-executable "/home/ptoharia/.emacs.d/clang.emacs")


(use-package all-the-icons
  :ensure t )

(use-package neotree
  :ensure t )
(global-set-key (kbd "C-c n") 'neotree-toggle)
; Icons wont work unless fonts are installed
; https://github.com/domtronn/all-the-icons.el/tree/master/fonts
; in Linux, copy the ttf to ~/.fonts directory
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))


(when (fboundp 'winner-mode)
  (winner-mode 1))

(load "~/.emacs.d/modeline.el")
(load "~/.emacs.d/tide.el")

(use-package sudo-edit                  ; Edit files as root, through Tramp
  :ensure t
  :defer t
  :bind (("C-c f s" . sudo-edit)
         ("C-c f S" . sudo-edit-current-file)))

(use-package goto-chg
  :ensure t )
(global-unset-key (kbd "C-c C-j"))
(global-set-key (kbd "C-c C-j") 'goto-last-change)



(defun concat-lines ()
  (interactive)
  (next-line)
  (join-line)
  (delete-horizontal-space))
(global-set-key (kbd "s-j") 'concat-lines )


(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "s-d") 'duplicate-line)


(use-package highlight-indent-guides
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'character)
)

(use-package git-gutter
  :ensure t )
(global-git-gutter-mode +1)
(git-gutter:linum-setup)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(git-gutter:update-interval 2)
 '(package-selected-packages
   (quote
    (fira-code-mode tide zop-to-char use-package smex smart-comment rtags rainbow-delimiters powerline paredit neotree move-text minimap markdown-mode magit imenu-anywhere ido-ubiquitous hl-line+ highlight-symbol goto-chg git-gutter ggtags flyspell-popup flycheck-clangcheck flx-ido fill-column-indicator ess easy-kill crux company-irony cmake-ide beacon auto-complete-clang anzu all-the-icons))))

;; (global-linum-mode +1)
(global-display-line-numbers-mode +1)
;; ;;; .init.el ends here
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
