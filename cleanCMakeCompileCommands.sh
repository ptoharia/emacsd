#!/bin/bash

cd $1 && cmake . && cp compile_commands.json compile_commands.json.orig && cat compile_commands.json.orig  | sed -e "s/ -fmax-errors=5//g" | sed -e 's/\\\\\\\"//g' > compile_commands.json
